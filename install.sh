#!/bin/sh

printUsage() {
    echo "\"install.sh\" requires exactly 2 arguments."
    echo ""
    echo "Usage:  ./install.sh NAMESPACE RUNNER_CONF_FILE"
    echo ""
    echo "Install Gitlab runner on k8s namespace"
}

if [ $# -eq 0 ]
  then
    printUsage
    exit 1
fi

# Add Gitlab repository
helm repo add gitlab https://charts.gitlab.io

# Install Gitlab runner on your k8s namespace
helm install --namespace $1 gitlab-runner -f $2 gitlab/gitlab-runner