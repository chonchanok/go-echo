package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func main() {

	e := echo.New()

	e.GET("/", hello)

	e.Logger.Printf("Server started at http://localhost:1323")
	e.Logger.Fatal(e.Start(":10000"))
}

func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Hi guys!")
}
