FROM golang:1.17-alpine

RUN apk update

WORKDIR /app

COPY . .

RUN go mod download

RUN go build -o main .

CMD [ "./main" ]